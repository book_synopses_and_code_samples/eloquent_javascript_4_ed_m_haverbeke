 #########
 data sets
 #########


 arrays
 ======

 Arrays are used for storing values (of the same type) in a sequential order.
 --------------------------------------------------------------------
 | var listOfNumbers = [1, 2, 3];                                   |            
 --------------------------------------------------------------------


 properties
 ==========

 Allmost all JS values have properties (excpet "null" and "undefined").
 The two most common ways to access properties in JS are 
 with a dot and with square brackets.

 When using a dot, the part after the dot must be a valid variable name,
 and it directly names the property.
 When unsing square brackets, the expression between the brackets is
 evaluated to get the property name.
 (And because property names can be any string, if you want to extract
  the property named "2" or "John Doe", you must use square brackets:
  "value[2]" or "value["John Doe"]".)

  The elements in an array are stored in properties. Because the names
  of these properties are numbers and we often need to get their name
  from a variable, we have to use the bracket syntax to access them.


 methods
 =======

 Properties that contain functions are generally called methods of
 the value they belong to.


 methods of arrays
 =================

 push(value)
 -----------
    adds a value to the end of an array

 pop()
 -----
    removes the value at the end of the array,
    and returns it

 join(separator)
 ---------------
    returnes a single string flattend from the array, where
    the "separator" is glued between the array's elements


 objects
 =======

 Values of the type "object" are arbitrary collections of properties.
 And we can add or remove these properties as we please.

 Inside the curly braces, we can give a list of properties separated by
 commas. Each property is written as a name, followed by a colon, followed
 by an expression that provides a value for the property. Spaces and line breaks
 are not significant. Properties whose names are not valid variable names or 
 valid numbers have to be quoted.
 --------------------------------------------------------------------
 | var descriptions = {                                             |
 |   work: "Went to work",                                          |
 |   "touched tree": "Touched a tree"                               |
 --------------------------------------------------------------------

 Reading a property that doesn't exist will produce the value "undefined".

 It is possible to assing a value to a property expression with the "="
 operator. This will replace the property'es value if it already existed
 or create a new property on the object if it didn't.


 JS operators for properties
 ===========================

 delete
 ------
    --------------------------------------------------------------------
    | delete anObject.property;                                        |
    --------------------------------------------------------------------
    the unary operator that removes the named property from the object.

 in
 --
    --------------------------------------------------------------------
    | if ("propertyName" in object) { ...                              |
     --------------------------------------------------------------------
    the binary operator, when applied to a string and an object,
    returns a Boolean value that indicated whether that object 
    has that property. 
 
 "=="
 ----

    JS's == operatro, when comparing objects, will return "true" only
    if both objects are precisely the same value. Compareing different
    objects will return "false", event if they have identical contents.
