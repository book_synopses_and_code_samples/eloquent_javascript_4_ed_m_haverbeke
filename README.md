Project information
-------------------

 This repository contains my files (my code snippets and my synopses about the main book's topics) 
 for the book **"eloquent javascript"** *2-th edition* by **Marijn Haverbeke**.

 It is free.
 
 [here is the link to it's site](http://eloquentjavascript.net/). 


Installation
------------

 Simply copy (clone the repository) and see and read it.

 
Basic usage
-----------
 
 `$ git clone https://gitlab.com/book_synopses_and_code_samples/eloquent_javascript_4_ed_m_haverbeke.git`

 
License
-------

 This project is offered under the MIT license.


Contributing
------------

 This project doesn't need to be contributed.

 But just in case you can always connect with me.